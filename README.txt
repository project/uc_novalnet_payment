﻿How to install:
~~~~~~~~~~~~~~
  Please follow the steps to do Übercart Novalnet payment gateway integration:
  ----------------------------------------------------------------------------
  1. Copy the module folder(uc_novalnet) to "/modules/ubercart/payment/".

  2. Login AS administer. then run the http://<server name>/update.php to make 
     the database changes.

  3. Visit admin/build/modules (Home › Administer › Site building) and activate
     Novalnet Payment methods whatever you want by selectingthat checkbox.
    (All payment methods stand for their own so you should activate Novalnet
    Core module with at least one payment method).
   
  4. Visit /admin/store/settings/novalnet
    (Home › Administer › Store administration › Configuration > Novalnet settings)
    and enter your Novalnet auth code, product id, tariff id, vendor id.

  5. Visit /admin/store/settings/payment/edit/methods
    (Home › Administer › Store administration › Configuration
    › Payment settings > Payment methods) and activate Novalnet Payment methods
    whatever you want by selecting that checkbox.

  6. To write the Schema Structure To the Database.
     Run the <server Url>/update.php.

  7.Now Novalnet Payment Modules are ready to Process.

  Note* :
  ------
  1) To include Novalnet Payment Details in customer order email/print invoice,
     a) If you are using 'customer' template, kindly do this
        in customer email template. (This facility already available
        on 'admin' template, so no need to edit that file)

     Customer Template file/path:
         "\modules\ubercart\uc_order\templates\uc_order-customer.tpl"
         Search the line, "t('Order Summary:')"
         and place the below code after the searched line

     <!--  Novalnet Order Details  - Start -->

         <br><?php print $order_comments; ?>

     <!--  Novalnet Order Details - End  -->

  2) Copy the file "callback_novalnet2ubercart3.php" and place it in shop's root folder.

       Kindly follow the below steps to test the callback script for Invoice and Prepayment payment methods:
       Url: (siteurl)/callback_novalnet2ubercart3.php
       [ Ex: https://ubercart3.novalnet.de/callback_novalnet2ubercart3.php ]

       Note* :
       -------

       To run the callback action script, kindly make the follwing changes in callback_novalnet2ubercart3.php
 
      a) $debug = TRUE; //set FALSE for live mode
      b) $callback_test_mode  = TRUE; //set FALSE for live mode
      c) formatted url :-
        <Site URL>/callback_novalnet2ubercart3.php?vendor_id=4&status=100&payment_type=INVOICE_CREDIT&tid_payment=12675800001204435&amount=3778&tid=12675800001204435&order_no=12

      Parameters:
      ----------

      tid          - Callscript Transaction TID
      vendor_id    - Merchant ID
      status       - Successfull payment transaction value
      payment_type - Types of payment process
      tid_payment  - Existing shops order transaction id
      amount       - Customer paid amount in cents
      order_no     - Order no for purchased products

      d) Mail Configuration :

      //Reporting Email Addresses Settings

      $mailHost      = 'Your Email Server';//adapt your mail host
      $mailPort      = 25;		    //adapt your mail host port

      //Test Data Settings

      if ($callback_test_mode){
          $emailFromName = "Novalnet test"; // Sender name, adapt
          $emailToName   = ""; 		    // Recipient name, adapt
          $emailToAddr 	 = 'Your Emailaddress'; //mandatory for test; adapt
          $emailFromAddr = 'Your Emailaddress'; //mandatory for test; adapt
          $emailSubject  = $emailSubject.' - TEST';//adapt
      }

     e)  Use this ip address:

	$ipAllowed = '195.143.189.210';

  Note: If you use prepayment and/or Invoice then contact us for more details.

  That's all. Your Shop is now Ready for Online Payment.

  On Any Technical Problems, 
  please contact sales@novalnet.de / 0049-89-923 068 320.

  -----------------------------------------------------------------------------
  Important Notice for Online Transfer (Sofortüberweisung):

  If you use real transaction data (bank code, bank account number, ect.) real
  transactions will be performed, even though the test mode is on/activated!
  ----------------------------------------------------------------------------
  If you do not have access to our admin tool "https://admin.novalnet.de" yet,
  you can perform tests nevertherless by using the following TEST data:

  Parameters (Which are to be entered in the admin interface)
     Novalnet Vendor ID       : 4
     Novalnet Vendor Auth-Code: JyEtHUjjbHNJwVztW6JrafIMHQvici
     Novalnet Product ID      : 13
     Novalnet Tariff ID	      : 127
     Password                 : a87ff679a2f3e71d9181a67b7542122c
     Paypal-API-Username      : technic-facilitator_api1.novalnet.de
     Paypal-API-Password      : 1363879539
     Paypal-API-Signature     : ACpWpnZ.LSW25rxxj7Q0FEPEzWxxAdlrxljqNS7c4h9Mj0f5sgruwbqX
