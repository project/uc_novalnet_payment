<?php
/**
 * Novalnet Callback Script for Ubercart
 *
 * NOTICE
 *
 * This script is used for real time capturing of parameters passed
 * from Novalnet AG after Payment processing of customers.
 *
 * This script is only free to the use for Merchants of Novalnet AG
 *
 * If you have found this script useful a small recommendation as well
 * as a comment on merchant form would be greatly appreciated.
 *
 * Please contact sales@novalnet.de for enquiry or info
 *
 * ABSTRACT: This script is called from Novalnet, as soon as a payment
 * done for payment methods, e.g. Prepayment, Invoice.
 * An email will be sent if an error occurs
 *
 *
 * @category   Novalnet
 * @package    Novalnet
 * @version    1.0
 * @copyright  Copyright (c) 2012 Novalnet AG. (http://www.novalnet.de)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @notice     1. This script must be placed in Ubercart root folder
 *                to avoid rewrite rules (mod_rewrite)
 *             2. You have to adapt the value of all the variables
 *                commented with 'adapt ...'
 *             3. Set $callback_test_mode/$debug to FALSE for live system
*/

define('DRUPAL_ROOT', getcwd());

require_once 'includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

//(loads everything, but doesn't render anything)
require_once('includes/module.inc');

//Table Names
define('ORDER_TABLE', 'uc_orders');
define('MSG_TABLE', 'uc_order_comments');
define('PAYMENT_REC_TABLE', 'uc_payment_receipts');

//Variable Settings
$debug = FALSE; //FALSE|TRUE; adapt: set to FALSE for go-live
$callback_test_mode = FALSE; //FALSE|TRUE; adapt: set to FALSE for go-live
$lineBreak = empty($_SERVER['HTTP_HOST'])? PHP_EOL: '<br />';
$addSubsequentTidToDb = TRUE;//whether to add the new tid to db; adapt if necessary

// Order State/Status Settings
    /*   Standard Types of Status:
        ~~~~~~~~~~~~~~~~~~~~~~~
        payment_received
        completed
        canceled
    */
$orderState  = "payment_received"; //Note: Indicates Payment accepted.

$aPaymentTypes = array('INVOICE_CREDIT');//adapt here if needed; Options are:
            /*
                COLLECTION_REVERSAL_AT
                COLLECTION_REVERSAL_DE
                CREDITCARD
                CREDITCARD_BOOKBACK
                CREDITCARD_CHARGEBACK
                CREDITCARD_REPRESENTMENT
                CREDIT_ENTRY_AT
                CREDIT_ENTRY_CREDITCARD
                CREDIT_ENTRY_DE
                DEBT_COLLECTION_AT
                DEBT_COLLECTION_CREDITCARD
                DEBT_COLLECTION_DE
                DIRECT_DEBIT_AT
                DIRECT_DEBIT_DE
                DIRECT_DEBIT_ES
                DIRECT_DEBIT_SEPA
                INVOICE
                INVOICE_CREDIT
                INVOICE_START
                NC_CONVERT
                NC_CREDIT
                NC_DEBIT
                NC_ENCASH
                NC_PAYOUT
                NOVALCARD
                NOVALTEL_DE
                NOVALTEL_DE_CB_REVERSAL
                NOVALTEL_DE_CHARGEBACK
                NOVALTEL_DE_COLLECTION
                ONLINE_TRANSFER
                PAYPAL
                PAYSAFECARD
                REFUND_BY_BANK_TRANSFER_EU
                RETURN_DEBIT_AT
                RETURN_DEBIT_DE
                REVERSAL
                WAP_CREDITCARD
                WAP_DIRECT_DEBIT_AT
                WAP_DIRECT_DEBIT_DE
            */

//Security Setting; only this IP is allowed for call back script
$ipAllowed = '195.143.189.210'; //Novalnet IP, is a fixed value, DO NOT CHANGE!!!!!

//Reporting Email Addresses Settings
$shopInfo      = 'Ubercart'.$lineBreak; //manditory;adapt for your need
$mailHost      = 'mail.novalnet.de';//adapt
$mailPort      = 25;//adapt
$emailFromAddr = '';//sender email addr., manditory, adapt it
$emailToAddr   = '';//recipient email addr., manditory, adapt it
$emailSubject  = 'Novalnet Callback Script Access Report'; //adapt if necessary;
$emailBody     = '';//Email text, adapt
$emailFromName = ""; // Sender name, adapt
$emailToName   = ""; // Recipient name, adapt

/*formatted url
<Site URL>/callback_novalnet2ubercart3.php?vendor_id=4&status=100&order_no=00000513&payment_type=INVOICE_CREDIT&tid_payment=12614100000408285&amount=19529&tid=12614100000408285

Parameters:

tid             Callscript Transaction TID
vendor_id       Merchant ID
status          Successfull payment transaction value
order_no        Existing shops order no which need to be update
payment_type    Types of payment process
tid_payment     Existing shops order transaction id
amount          Customer paid amount in cents
*/

//Parameters Settings
$hParamsRequired = array(
  'vendor_id'    => '',
  'tid'          => '',
  'payment_type' => '',
  'status'       => '',
  'amount'       => '',
  'tid_payment'  => '');

$hParamsTest = array(
  'vendor_id'    => '4',
  'status'       => '100',
  'amount'       => '1.0',//must be avail. in shop database; 850 = 8.50
  'payment_type' => 'INVOICE_CREDIT',
  'tid_payment'  => '12677600001501796',//orig. tid; must be avail. in shop database
  'tid'          => '12345678901234567',//subsequent tid, from Novalnet backend; can be a fake for test
);

if (in_array('INVOICE_CREDIT', $aPaymentTypes) and isset($_REQUEST['payment_type']) and $_REQUEST['payment_type'] == 'INVOICE_CREDIT'){
  $hParamsRequired['tid_payment'] = '';
  $hParamsTest['tid_payment'] = '12677600001501796'; //orig. tid; must be avail. in shop database; adapt for test;
}
ksort($hParamsRequired);

//Test Data Settings
if ($callback_test_mode) {
  $emailFromName = "Novalnet"; // Sender name, adapt
  $emailToName   = "Novalnet"; // Recipient name, adapt
  $emailFromAddr = '';//manditory for test; adapt
  $emailToAddr   = '';//manditory for test; adapt
  $emailSubject  = $emailSubject.' - TEST';//adapt
}

// ################### Main Prog. ##########################
try {
  //Check Params
  if (checkIP($_REQUEST)) {
    if (checkParams($_REQUEST)) {
      getIncrementId($_REQUEST);
    }
  }
  if (!$emailBody) {
    $emailBody .= 'Novalnet Callback Script called for StoreId Parameters: ' . print_r($_POST, TRUE) . $lineBreak;
    $emailBody .= 'Novalnet callback succ. ' . $lineBreak;
    $emailBody .= 'Params: ' . print_r($_REQUEST, TRUE) . $lineBreak;
  }
}catch(Exception $e) {
   $emailBody .= "Exception catched: $lineBreak\$e:" . $e->getMessage() . $lineBreak;
}

if ($emailBody) {
  if (!sendEmail($emailBody)) {
    if ($debug) {
      echo "Mailing failed!".$lineBreak;
      echo "This mail text should be sent: ".$lineBreak;
      echo $emailBody;
    }
  }
}

// ############## Sub Routines #####################

//Function to send mail
function sendEmail($emailBody) {
  global $lineBreak, $debug, $callback_test_mode, $emailFromAddr, $emailToAddr, $emailFromName, $emailToName, $emailSubject, $shopInfo, $mailHost, $mailPort;
  //Send Email
   $emailBodyT = str_replace('<br />', PHP_EOL, $emailBody);

  ini_set('SMTP', $mailHost);
  ini_set('smtp_port', $mailPort);

  header('Content-Type: text/html; charset=iso-8859-1');
  $headers = 'From: ' . $emailFromAddr . "\r\n";

  try {
    if ($debug) {
      echo __FUNCTION__.' : Sending Email suceeded!'.$lineBreak;
    }
    $sendmail = mail($emailToAddr, $emailSubject, $emailBodyT, $headers);
  }
  catch(Exception $e) {
    if ($debug) {echo 'Email sending failed: '.$e->getMessage();}
    return FALSE;
  }
  if ($debug) {
    echo 'This text has been sent:'.$lineBreak.$emailBody;
  }
  return TRUE;
}

//Function to check values stored in basic parameters
function checkParams($_REQUEST) {
  global $lineBreak, $hParamsRequired, $emailBody, $aPaymentTypes;
  $error = FALSE;
  $emailBody = '';
  if (!$_REQUEST) {
    $emailBody .= 'Novalnet callback received. No params passed over!' . $lineBreak;
    return FALSE;
  }

  if (!isset($_REQUEST['payment_type'])){
    $emailBody .= 'Novalnet callback received. But Param payment_type missing' . $lineBreak;
    return FALSE;
  }

  //Only Payment Type 'INVOICE_CREDIT' allowed; Otherwise you have to adapt the logic
  if (!in_array($_REQUEST['payment_type'], $aPaymentTypes)){
    $emailBody .= "Novalnet callback received. But passed payment_type (" . $_REQUEST['payment_type'] . ") not defined in \$aPaymentTypes: (" . implode('; ', $aPaymentTypes) . ")" . $lineBreak;
    return FALSE;
  }

  if ($hParamsRequired) {
    foreach ($hParamsRequired as $k=>$v) {
      if (empty($_REQUEST[$k])) {
        $error = TRUE;
        $emailBody .= 'Required param (' . $k . ') missing!' . $lineBreak;
      }
    }
    if ($error) {
      return FALSE;
    }
  }

  //Check the novalnet transaction status code
  if(!isset($_REQUEST['status']) or 100 != $_REQUEST['status']) {
    $emailBody .= 'Novalnet callback received. Status [' . $_REQUEST['status'] . '] is not valid: Only 100 is allowed.' . $lineBreak . $lineBreak . $lineBreak;
    return FALSE;
  }

  //Check the novalnet payment tid for order
  if(strlen($_REQUEST['tid_payment'])!=17){
    $emailBody .= 'Novalnet callback received. Invalid TID [' . $_REQUEST['tid_payment'] . '] for Order.' . $lineBreak . $lineBreak . $lineBreak;
    return FALSE;
  }

  //Check the novalnet subsequent tid
  if(strlen($_REQUEST['tid'])!=17){
    $emailBody .= 'Novalnet callback received. New TID is not valid.' . $lineBreak . $lineBreak . $lineBreak;
    return FALSE;
  }

  return TRUE;
}

//Function to load the order id for the given TID.
function getIncrementId($_REQUEST) {
    global $lineBreak, $emailBody, $debug, $oDB, $orderDetails;
    $orderDetails = array();

    //check amount
    $amount  = $_REQUEST['amount'];
    $_amount = isset($order_total) ? $order_total * 100 : 0;

    if(!$amount || $amount < 0) {
        $emailBody .= "Novalnet callback received. The requested amount ($amount) must be greater than zero." . $lineBreak . $lineBreak;
        return FALSE;
    }
   $uc_order = db_select('uc_order_comments', 'arg')
                  -> fields('arg', array('order_id'))
                  -> condition('message', '%'. $_REQUEST['tid_payment'] .'%' ,'like')
                  -> execute()-> fetchObject();

    $order_no = (isset($_REQUEST['order_no']) && !empty($_REQUEST['order_no'])) ? $_REQUEST['order_no'] : '' ;
    if (!$uc_order) {
        $emailBody .= 'Novalnet callback received. Payment type is not Prepayment/Invoice!';
        $emailBody .= $lineBreak . $lineBreak . $lineBreak;
        return FALSE;
    }
    else {
        if($order_no != '') {
            if($uc_order->order_id == $order_no){
                $uc_orderId  = $uc_order->order_id;
            }else{
                $emailBody .= 'Novalnet callback received. Payment type is not Prepayment/Invoice!';
                $emailBody .= $lineBreak . $lineBreak . $lineBreak;
                return FALSE;
            }
        }
        else {
            $uc_orderId  = $uc_order->order_id;
        }

    }
    $orderDetails = uc_order_load($uc_orderId);

    if (empty($uc_orderId) or !$orderDetails) {
        $emailBody .= 'Novalnet callback received. Payment type is not Prepayment/Invoice!';
        $emailBody .= $lineBreak . $lineBreak . $lineBreak;
        return FALSE;
    }

  //check amount
  $amount  = $_REQUEST['amount'];
  $store_price = $orderDetails->order_total;
  $roundofprice = round($store_price, 2);
  $final_price = $roundofprice * 100;
  $_amount = isset($final_price) ? $final_price : 0;

  $odetailsdisp = 'Order Details:<pre>Order No:'.$uc_orderId.$lineBreak.'Amount : '.$roundofprice.'&nbsp;'.$orderDetails->currency.'</pre>';

  if ($debug) { echo $odetailsdisp;}

  $paymentType = $orderDetails->payment_method;
  if (!in_array($paymentType, array('uc_novalnet_invoice', 'uc_novalnet_prepayment'))) {
    $emailBody .= "Novalnet callback received. Payment type ($paymentType) is not Prepayment/Invoice!$lineBreak$lineBreak";
    return FALSE;
  }
  setOrderStatus($uc_orderId, $orderDetails);
}

//Function to update the order details like comments, order status and amount.
function setOrderStatus ($incrementId, $orderDetails) {
  global $lineBreak, $createInvoice, $emailBody, $orderStatus, $orderState, $addSubsequentTidToDb, $oDB;

  if ($incrementId) {
      if ($orderDetails->order_status != 'payment_received') {
        $callback_comment = " Novalnet Callback Script executed successfully. The subsequent TID: (".$_REQUEST["tid"].") on ".date("Y-m-d H:i:s");
        $rec_time = time();
        $method = ($orderDetails->payment_method) == 'uc_novalnet_invoice' ? t('Novalnet Invoice') : t('Novalnet Prepayment');
        $receipt_id = db_insert('uc_payment_receipts')
    ->fields(array(
      'order_id' => $incrementId,
      'method' => $method,
      'amount' => $orderDetails->order_total,
      'uid' => $orderDetails->uid,
      'data' => NULL,
      'comment' => $callback_comment,
      'received' => $rec_time,
    ))
    ->execute();
        uc_order_update_status($incrementId, $orderState);
        uc_order_comment_save($incrementId,  0,  $callback_comment ,  'order',  $orderState, 'TRUE');
      }
      else{
        $emailBody .= "Novalnet callback received. Callback Script executed already. Refer Order :".$incrementId;
        return FALSE;
      }
  }
  else {
    $emailBody .= "Novalnet Callback received. No order for Increment-ID $incrementId found.";
    return FALSE;
  }
  $emailBody .= 'Novalnet Callback Script executed successfully. Payment for order id: ' . $incrementId . '. New TID: ('. $_REQUEST['tid'] . ') on ' . date('Y-m-d H:i:s');
  return TRUE;
}

//Function to validate the ip address
function checkIP($_REQUEST) {
  global $lineBreak, $ipAllowed, $callback_test_mode, $emailBody;
  if ($callback_test_mode) { $ipAllowed = getRealIpAddr(); }

  $callerIp  = ip_address();

  if ($ipAllowed != $callerIp) {
    echo 'Novalnet callback received. Unauthorised access from the IP [' . $callerIp . ']' . $lineBreak . $lineBreak;
    exit;
  }
  return TRUE;
}
?>
